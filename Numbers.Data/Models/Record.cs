﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers.Data.Models
{
    public enum Order { Ascending, Descending }

    public class Record
    {
        public int Id { set; get; }

        [Required]
        public string Numbers { set; get; }

        public double TimeToSort { set; get; }

        public Order Order { set; get; }
    }
}
