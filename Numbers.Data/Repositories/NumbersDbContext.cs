﻿using Microsoft.EntityFrameworkCore;
using Numbers.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Numbers.Data.Repositories
{
    public class NumbersDbContext : DbContext
    {
        public DbSet<Record> Records { set; get; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .EnableSensitiveDataLogging(true)
                //.UseSqlite("Filename=NumbersDatabase.db");
                .UseSqlServer(@"Server=(localdb)\mssqllocaldb; Database=NumbersDatabase; Trusted_Connection=True;ConnectRetryCount=0");
        }

        public void Initialise()
        {
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        public void Delete()
        {
            Database.EnsureDeleted();
        }
    }
}
