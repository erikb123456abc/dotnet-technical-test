﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Numbers.Data.Models;
using Numbers.Data.Repositories;

namespace Numbers.Data.Service
{
    public class NumbersService
    {
        private readonly NumbersDbContext db;

        public NumbersService()
        {
            db = new NumbersDbContext();
        }

        public void Delete()
        {
            db.Delete();
        }

        public void Initialise()
        {
            //call to Initialise() method from DbContext:
            db.Initialise();
        }

        public Record AddRecord(Record r)
        {
            if (r == null)
                return null;
            db.Add(r);
            db.SaveChanges();
            if (GetRecord(r.Id).Equals(r))
                return r;
            return null;
        }

        public List<Record> GetAllRecords(string orderBy = null)
        {
            var output = db.Records.ToList();
            if (orderBy == "Ascending")
            {
                output = db.Records.OrderBy(s => s.TimeToSort).ToList();
            }

            if (orderBy == "Descending")
            {
                output = db.Records.OrderByDescending(s => s.TimeToSort).ToList();
            }
            return output;
        }

        public Record GetRecord(int id)
        {
            var record = db.Records.Where(s => s.Id == id).SingleOrDefault();
            if (record == null)
                return null;
            return record;
        }
    }
}
