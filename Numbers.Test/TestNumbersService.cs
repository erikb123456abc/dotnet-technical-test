using Numbers.Data.Service;
using Xunit;
using Numbers.Data.Models;
using System.Linq;

namespace Numbers.Test
{
    public class TestNumbersService
    {
        private readonly NumbersService svc;

        public TestNumbersService()
        {
            // general arrangement
            svc = new NumbersService();
            svc.Delete();
            svc.Initialise();
        }

        [Fact]
        public void AddRecord_WithNullObject_ShouldReturnNull()
        {
            var output = svc.AddRecord(null);

            Assert.Null(output);
        }

        [Fact]
        public void AddRecord_WithRecordObject_ShouldReturnObject()
        {
            var r = new Data.Models.Record { Numbers = "1 2 3 4", Order = Order.Ascending, TimeToSort = 0.5 };

            var output = svc.AddRecord(r);

            Assert.Equal(r, output);
        }

        [Fact]
        public void AddRecord_With2RecordObjects_ShouldReturn2Objects()
        {
            var r1 = new Data.Models.Record { Numbers = "2 3 4 5", Order = Order.Ascending, TimeToSort = 1 };
            var r2 = new Data.Models.Record { Numbers = "11 9 8 3", Order = Order.Descending, TimeToSort = 2 };

            svc.AddRecord(r1);
            svc.AddRecord(r2);

            var output = svc.GetAllRecords("Ascending").Count();

            Assert.Equal(2, output);
        }

        [Fact]
        public void GetAllRecords_WithNoRecordObjects_ShouldReturnZero()
        {
            var output = svc.GetAllRecords("Ascending").Count();

            Assert.Equal(0,output);
        }

        [Fact]
        public void GetAllRecords_WithOneRecordObject_ShouldReturnObject()
        {
            var r = new Data.Models.Record { Numbers = "0 0 0 0", TimeToSort = 0.1 };

            svc.AddRecord(r);

            var list = svc.GetAllRecords("Descending");

            var record = list[0];

            var output = record.TimeToSort;

            Assert.Equal(0.1, output);
        }

        [Fact]
        public void GetAllRecords_AscendingWithThreeRecordObjects_ShouldReturnSecondObject()
        {
            var r1 = new Data.Models.Record { Numbers = "1", TimeToSort = 1 };
            var r2 = new Data.Models.Record { Numbers = "0", TimeToSort = 1.2 };
            var r3 = new Data.Models.Record { Numbers = "2", TimeToSort = 1.1 };

            svc.AddRecord(r1);
            svc.AddRecord(r2);
            svc.AddRecord(r3);

            var list = svc.GetAllRecords("Ascending");

            var record = list[2];

            var output = record.TimeToSort;

            Assert.Equal(1.2, output);
        }

        [Fact]
        public void GetAllRecords_DescendingWithThreeRecordObjects_ShouldReturnLastObject()
        {
            var r1 = new Data.Models.Record { Numbers = "1", TimeToSort = 1 };
            var r2 = new Data.Models.Record { Numbers = "0", TimeToSort = 1.2 };
            var r3 = new Data.Models.Record { Numbers = "2", TimeToSort = 1.1 };

            svc.AddRecord(r1);
            svc.AddRecord(r2);
            svc.AddRecord(r3);

            var list = svc.GetAllRecords("Descending");

            var record = list[2];

            var output = record.TimeToSort;

            Assert.Equal(1, output);
        }

        [Fact]
        public void GetRecord_WithNoRecordObjects_ShouldReturnNull()
        {
            var output = svc.GetRecord(1);

            Assert.Null(output);
        }

        [Fact]
        public void GetRecord_WithTwoRecordObjects_ShouldReturnSecondObject()
        {
            var r1 = new Data.Models.Record { Numbers = "1 0 1", TimeToSort = 2 };
            var r2 = new Data.Models.Record { Numbers = "0 1 0", TimeToSort = 1 };

            svc.AddRecord(r1);
            svc.AddRecord(r2);

            var record = svc.GetRecord(2);

            Assert.Equal(r2, record);
        }
    }
}
