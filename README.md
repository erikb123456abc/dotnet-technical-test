•	solution was built using .NET Core 2.2 - although very similar, I am not familiar with .NET Framework; I hope this is acceptable
•	the sorting time is stored & displayed in milliseconds
•	it's assumed that the number of integers entered for sorting at any one time will not exceed 100,000; the solution has not been tested for larger inputs
•	it's assumed that the numbers entered are integers, i.e. not containing a fractional part, and within the minimum and maximum values for the integer data type
•	service methods have been tested using XUnit 
