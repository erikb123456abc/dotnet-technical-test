﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using EFMovie.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Numbers.Data.Models;
using Numbers.Data.Service;
using Solution.Web.Models;


namespace WebApplication1.Controllers
{
    public class RecordController : BaseController
    {

        private readonly NumbersService svc;

        public RecordController()
        {
            svc = new NumbersService();
        }

        public ViewResult Index(string order = null)
        {
            var records = svc.GetAllRecords(order);

            return View(records);
        }

        public ViewResult GetRecord(int id)
        {
            var record = svc.GetRecord(id);
            var records = svc.GetAllRecords("Ascending");
            if (record==null)
            {
                Alert("Numbers not found", AlertType.warning);
                return View(nameof(Index), records);
            }
            return View(record);
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Export(string order)
        {

            var records = svc.GetAllRecords("Ascending");

            if (order == "Descending")
            {
                records = svc.GetAllRecords("Descending");
            }

            if (records.Count()==0)
            {
                Alert("The database is empty, there is no data to export!", AlertType.warning);
                return View(nameof(Index),records);
            }

            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(Record));
                TextWriter txtWriter = new StreamWriter("OrderedNumbers.xml");

                foreach (var r in records)
                {
                    xs.Serialize(txtWriter, r);
                }

                txtWriter.Close();
                Alert("Output successful, see file OrderedNumbers.xml", AlertType.success);
                return View(nameof(Index), records);

            } catch (Exception)
            {
                Alert("Output to XML file failed!", AlertType.warning);
                return View(nameof(Index), records);
            }
        }

        [HttpPost]
        public IActionResult Create([Bind("Numbers,TimeToSort,Order")] Record r)
        {
            var numbersString = r.Numbers;
            var intList = new List<int>();
            var stringsList = new List<string>();
            try
            {
                numbersString = r.Numbers.Trim();

                stringsList = numbersString.Split(' ').ToList();
                intList = new List<int>();
                for (int i = 0; i < stringsList.Count; i++)
                {
                    var stringnum = stringsList[i];
                    var temp = Convert.ToInt32(stringnum);
                    intList.Add(temp);
                }

            } catch ( Exception )
            {
                Alert("Invalid input: please enter integers only, separated by one space", AlertType.warning);
                return View(nameof(Create));          
            }

            IEnumerable sorted = intList;
            long elapsedTicks=0;

            if (r.Order == Order.Ascending)
            {
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                sorted = intList.OrderBy(s => s);
                stopWatch.Stop();
                elapsedTicks = stopWatch.ElapsedTicks;
            }

            if (r.Order == Order.Descending)
            {
                Stopwatch stopWatch2 = new Stopwatch();
                stopWatch2.Start();
                sorted = intList.OrderByDescending(s => s);
                stopWatch2.Stop();
                elapsedTicks = stopWatch2.ElapsedTicks;
            }

            string output = "";

            foreach (var v in sorted)
            {
                string vs = v.ToString();
                output = String.Concat(output, vs," ");
            }

            output.Trim();

            r.Numbers=output;
            var elapsedMs = (double) elapsedTicks / Stopwatch.Frequency * 1000;
            r.TimeToSort = elapsedMs;
            svc.AddRecord(r);

            Alert("Numbers successfully sorted & added to database!", AlertType.success);

            return View(nameof(GetRecord),r );
        }
    }
}
